package com.nawapat.week10;

public class Rectangle extends Shape {
    private double width;
    private double hight;

    public Rectangle(double width, double hight) {
        super("Rectangle");
        this.width = width;
        this.hight = hight;
    }

    public double getWidth() {
        return width;
    }

    public double getHight() {
        return hight;
    }

    @Override
    public String toString() {
        return this.getName() + "  width : " + this.width + ", hight : " + this.hight;
    }

    @Override
    public double calArea() {
        return this.width * this.hight;
    }

    @Override
    public double calPerimeter() {
        return (this.width + this.hight) * 2;
    }
}
