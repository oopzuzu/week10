package com.nawapat.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.println("Area of a Rectangle is " + rec1.calArea());
        System.out.println("Perimeter of a Rectangle is " + rec1.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println("Area of a Rectangle is " + rec2.calArea());
        System.out.println("Perimeter of a Rectangle is " + rec2.calPerimeter());

        System.err.println();
        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area : %.3f \n", circle1.getName(), circle1.calArea());
        System.out.printf("%s perimeter : %.3f \n", circle1.getName(), circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area : %.3f \n", circle2.getName(), circle2.calArea());
        System.out.printf("%s perimeter : %.3f \n", circle2.getName(), circle2.calPerimeter());

        System.err.println();
        Triangle triangle1 = new Triangle(2, 2, 2);
        System.out.println(triangle1);
        System.out.printf("%s area : %.3f \n", triangle1.getName(), triangle1.calArea());
        System.out.printf("%s perimeter : %.3f \n", triangle1.getName(), triangle1.calPerimeter());

    }
}
